#!/bin/sh -e

VERSION=$2
TAR=../squirrel-sql_$VERSION+dfsg.orig.tar.gz
DIR=squirrel-sql-$VERSION+dfsg
SVN=https://squirrel-sql.svn.sourceforge.net/svnroot/squirrel-sql/tags
GIT=git://git.code.sf.net/p/squirrel-sql/git
TAG=$(echo "squirrelsql-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

# clean up the upstream tarball
git clone --bare "$GIT" git-tmp-dir
(cd git-tmp-dir && git archive --format=tar --prefix="$DIR/" "$TAG") | tar -xf -
rm -rf git-tmp-dir
#svn export $SVN/$TAG $DIR
tar -c -z -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
